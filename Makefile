CPP := g++
FLAGS := -Wall -Werror -std=c++17

all: main
	./main

%: %.cpp
	${CPP} ${FLAGS} -o $@ $<
